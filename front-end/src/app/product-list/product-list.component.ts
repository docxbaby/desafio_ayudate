import { Component, NgIterable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent {

  pIterables: NgIterable<any> = [];
  propertys: NgIterable<any> = [];
  arr: any[] = [];
  newArr: any[] = [];
  

  constructor(http: HttpClient) {

    http.get('http://localhost:3000/products').subscribe(
      {
        next: (res) => {
          let pArr = Array.from(Object.entries(res));
          let aArr = [];
          for (let i = 0; i < pArr.length; i++) {


            aArr.push(pArr[i][1])
          }
          this.arr = aArr;
          this.propertys = Object.keys(aArr[0]);

          this.pIterables = aArr;
        }
      }
    )
  }

  orderBy(event: Event) {
    let by = (event.target as HTMLSelectElement).value;
    let auxArray: any[] = [];
    let ordered: any[] = [];
    this.newArr = []


    for (let i = 0; i < this.arr.length; i++) {
      auxArray.push(this.arr[i][by]);
    }

    auxArray.sort();

    for (let i = 0; i < this.arr.length; i++) {
      for (let j = 0; j < auxArray.length; j++) {
        if (this.arr[i][by] == auxArray[j]) {
          ordered.push(j);
        }
      }

    }

    for (let i = 0; i < ordered.length; i++) {
      this.newArr.push(this.arr[ordered[i]])
    }

    this.pIterables = this.newArr;
  }

  finded: any[] = [];

  search(event: Event) {
    
    let value = (event.target as HTMLInputElement).value;
    for (let i = 0; i < this.arr.length; i++) {
      let str = this.arr[i][this.typeSearch].toString();
      value = value.toLocaleLowerCase();
      let regex = new RegExp(value, 'i')
      let match = str.match(regex);

      if (match != null) {
        this.finded.push( this.arr[i] )
      }

    }

    let toPop = (this.finded.length/2); 
    this.pIterables = this.finded
  }

  typeSearch: string = "title";

  setTypeSearch(event: Event) {
    let type = (event.target as HTMLSelectElement).value;
    this.typeSearch = type
  }

  //muestra el modal para modificar el producto 
  showModifier: boolean = false; 
  setShowModifier(prod:any){
    this.showModifier = !this.showModifier; 
    const { id } = prod;
    this.prodMod = id;
   
  }

  //current product to modify 
  prodMod:number = -1;

  //guarda las modificaciones del producto  
  saveProduct(){
    this.showModifier = !this.showModifier; 
  }

  closeModifier(){
    this.showModifier = !this.showModifier; 
  }  

}
