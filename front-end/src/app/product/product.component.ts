import { Component, EventEmitter, Input, NgIterable, Output } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent {
  @Input() data:NgIterable<any> = [];
  @Input() fields:NgIterable<any> = [];
  @Input() product: Product = new Product(); 
  @Input() name: string = ""; 
  @Output() cancel: EventEmitter<any> = new EventEmitter(); 

  constructor(){
  }
  
  cancelModify(){
    this.cancel.emit();
  }

  //tendra los datos a actualizar del producto 
  prodNewData:Product = new Product();

  setValueFromInput( field:string, event:Event){
    let value = (event.target as HTMLInputElement).value;
    this.prodNewData[field] = value

  }

  saveChanges(){
    for (let i in this.prodNewData){
      if ( !!this.prodNewData[i] ) {
        this.product[i] = this.prodNewData[i];
      }
    }
    this.cancel.emit();
  }

  
}

class Product {
  id:number;
  title:string;
  description:string;
  price:number; 
  discountPercentage:number; 
  rating:number; 
  stock:number; 
  brand:string; 
  category:string; 
  thumbnail:string; 
  images:string; 
  [key: string]: any; // Index signature

  constructor(){
    this.id = 0;
    this.title = "";
    this.description = ""; 
    this.price = 0; 
    this.discountPercentage = 0;   
    this.rating = 0; 
    this.stock = 0 ;
    this.brand = ""; 
    this.category = ""; 
    this.thumbnail = ""; 
    this.images = ""; 
}
}