import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  constructor(private http: HttpClient) { }

  user: any
  pass: any

  @Output() loginEvent = new EventEmitter<string>();


  tryLogin() {
    this.http.get('http://localhost:3000/login', {
      params: {
        "username": this.user, "password": this.pass
      }
    }).subscribe({
      next: (response: any) => {
      },
      error: (error: any) => {
        // Handle errors
        console.error(error);
      },
      complete: () => {
        // Optional: Perform any cleanup or completion tasks
        this.loginEvent.emit("isLogged")
      }
    });

  };

  ngOnInit(): void {


  }


}
