const express = require('express');
const app = express();
const cors = require('cors');
const https = require('https');


// Enable CORS for all routes
app.use(cors());

app.use(cors({
    origin: 'http://localhost:4200'
}));

let products = {};


app.get('/login', (req, res) => {
    if (req.query.username == 'juan' && req.query.password == '123') {
        res.json({ "status": 200, "message": "Ingreso" })

        https.get("https://dummyjson.com/products", (res) => {
            let data = '';

            //agregrando info
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                products =  JSON.parse(data); 
            });

        }).on('error', (error) => {
            console.error(error);
        });
    }
});

app.get('/products', (req, res) => {
    res.send(products.products);
    console.log(products.products)
});


app.get('/', (req, res) => {
    res.send('Ruta principal');
});

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});

